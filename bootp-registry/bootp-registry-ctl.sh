#!/bin/bash -e

# Print an error message and exit with error status 1
print_critical() {
    >&2 printf "\e[91m[CRIT] %s\e[0m\n" "$@"
    exit 1
}

# Print an error message
print_error() {
    >&2 printf "\e[91m[ERRO] %s\e[0m\n" "$@"
}

# Print a warning message
print_warning() {
    >&2 printf "\e[93m[WARN] %s\e[0m\n" "$@"
}

# Print a note message
print_note() {
    printf "[NOTE] %s\n" "$@"
}

# Print an info message
print_info() {
    printf "\e[92m[INFO] %s\e[0m\n" "$@"
}

if [ ! -f /opt/livebootp/etc/config.yml ]; then
    print_critical "File /opt/livebootp/etc/config.yml not found"
fi

# YAML to JSON conversion
LIVEBOOTP_CONFIG="$(cat /opt/livebootp/etc/config.yml \
    | python -c 'import sys, yaml, json; json.dump(yaml.load(sys.stdin), sys.stdout)')"
config_query() {
    jq "$@" <<< "$LIVEBOOTP_CONFIG"
}
print_info "Read configuration file: /opt/livebootp/etc/config.yml"

# Read DNS property
if [ $(config_query -r '."bootp-registry"."domain-name-servers" | length') -gt 0 ]; then
    LIVEBOOTP_DNS=($(config_query -r '."bootp-registry"."domain-name-servers"[]'))
else
    LIVEBOOTP_DNS=($(cat /etc/resolv.conf | awk '/^nameserver/ {
            if ($2 !~ /^127\..*/ && $2 != "::1" ) {
                printf(" %s", $2);
            }
        }' 2> /dev/null || true))
fi
if [ ${#LIVEBOOTP_DNS[@]} -eq 0 ]; then
    print_critical "No valid DNS found: please provide ."bootp-registry".domain-name-servers config property"
fi
print_note "DNS list                     : ${LIVEBOOTP_DNS[*]}"

# Read domain name property
if [ $(config_query -r '."bootp-registry"."domain-name" | length') -gt 0 ]; then
    LIVEBOOTP_DN="$(config_query -r '."bootp-registry"."domain-name"')"
else
    LIVEBOOTP_DN="example.com"
fi
print_note "Root domain name             : $LIVEBOOTP_DN"

# Read registry domain property
if [ $(config_query -r '."bootp-registry"."targets-domain-name" | length') -gt 0 ]; then
    LIVEBOOTP_CN="$(config_query -r '."bootp-registry"."targets-domain-name"')"
else
    LIVEBOOTP_CN="bootp-registry.$LIVEBOOTP_DN"
fi
print_note "Registry domain name         : $LIVEBOOTP_CN"

# Read listen interface property
if [ $(config_query -r '."bootp-registry"."listen-intf" | length') -gt 0 ]; then
    LIVEBOOTP_INTF="$(config_query -r '."bootp-registry"."listen-intf"')"
else
    LIVEBOOTP_INTF="$(ip -4 route show | awk '/^default / {print $5; exit;}')"
fi
print_note "Listen on interface          : $LIVEBOOTP_INTF"

# Read routers property
if [ $(config_query -r '."bootp-registry"."routers" | length') -gt 0 ]; then
    LIVEBOOTP_ROUTER=($(config_query -r '."bootp-registry"."routers"[]'))
else
    LIVEBOOTP_ROUTER="$(ip -4 route show | awk '/^default / {print $3; exit;}')"
fi
print_note "Using router                 : $LIVEBOOTP_ROUTER"

# Read bootp-registry address
if [ $(config_query -r '."bootp-registry".address | length') -gt 0 ]; then
    LIVEBOOTP_ADDR="$(config_query -r '."bootp-registry".address')"
else
    LIVEBOOTP_ADDR="$(ip -4 addr show dev $LIVEBOOTP_INTF | awk '/^    inet / { print $2}')"
fi
print_note "Server adress                : $LIVEBOOTP_ADDR"

cdr2mask() {
   # Number of args to shift, 255..255, first non-255 byte, zeroes
   set -- $(( 5 - ($1 / 8) )) 255 255 255 255 $(( (255 << (8 - ($1 % 8))) & 255 )) 0 0 0
   [ $1 -gt 1 ] && shift $1 || shift
   echo ${1-0}.${2-0}.${3-0}.${4-0}
}

netaddr() {
    IFS=. read -r i1 i2 i3 i4 <<< $1
    IFS=. read -r m1 m2 m3 m4 <<< $2
    printf "%d.%d.%d.%d\n" "$((i1 & m1))" "$((i2 & m2))" "$((i3 & m3))" "$((i4 & m4))"
}

# Get network mask
LIVEBOOTP_NETMASK=$(cdr2mask ${LIVEBOOTP_ADDR#*/})
print_note "Server netmask               : $LIVEBOOTP_NETMASK"

# Get network adress
LIVEBOOTP_IP=${LIVEBOOTP_ADDR%/*}
LIVEBOOTP_NETADDR=$(netaddr $LIVEBOOTP_IP $LIVEBOOTP_NETMASK)
print_note "Network adress               : $LIVEBOOTP_NETADDR"

# Read ssh keys url property
if [ $(config_query -r '."bootp-registry"."ssh-authorized-keys-base-url" | length') -gt 0 ]; then
    LIVEBOOTP_SSH_AUTHORIZED_KEYS_BASE_URL="$(config_query -r '."bootp-registry"."ssh-authorized-keys-base-url"')"
else
    LIVEBOOTP_SSH_AUTHORIZED_KEYS_BASE_URL="http://$LIVEBOOTP_IP/ssh_authorized_keys"
fi
print_note "SSH authorized keys base URL : $LIVEBOOTP_SSH_AUTHORIZED_KEYS_BASE_URL"

# Read image url property
if [ $(config_query -r '."bootp-registry"."image-repo-url" | length') -gt 0 ]; then
    LIVEBOOTP_SQUASHFS_REPO_URL="$(config_query -r '."bootp-registry"."image-repo-url"')"
else
    LIVEBOOTP_SQUASHFS_REPO_URL="http://$LIVEBOOTP_IP/images"
fi
print_note "SquashFS Image repo URL      : $LIVEBOOTP_SQUASHFS_REPO_URL"

LIVEBOOTP_DHCPD_CONF="/etc/dhcp/dhcpd.conf"
LIVEBOOTP_TFTPROOT="/var/lib/tftpboot"
LIVEBOOTP_SSH_HOST_CONFIG="$LIVEBOOTP_TFTPROOT/ssh_host_config.txt"
LIVEBOOTP_PXELINCFG="$LIVEBOOTP_TFTPROOT/pxelinux.cfg"

# Generate dhcpd.conf head
cat > $LIVEBOOTP_DHCPD_CONF << EOF
ddns-update-style none;
option domain-name "$LIVEBOOTP_DN";
option domain-name-servers $(IFS=$','; echo "${LIVEBOOTP_DNS[*]}");
default-lease-time 3600;
max-lease-time 86400;
authoritative;
log-facility local7;
subnet $LIVEBOOTP_NETADDR netmask $LIVEBOOTP_NETMASK {
    option routers $(IFS=$','; echo "${LIVEBOOTP_ROUTER[*]}");
    next-server $LIVEBOOTP_IP;
    filename "pxelinux.0";
EOF

URL_PATTERN='^\w+://'
> "$LIVEBOOTP_SSH_HOST_CONFIG"

for host in $(config_query -r '. | keys[]' | grep -E -v '^(^\..*|bootp\-registry)$'); do
    print_note "Reading host $host..."
    host_cfg_state="ok"
    prefix=".\"$host\""
    hwaddr=$(config_query -r "$prefix"'."hardware-ethernet"')
    address=$(config_query -r "$prefix"'."fixed-address"')
    image=$(config_query -r "$prefix"'."image-name"')
    ssh_auth_keys=$(config_query -r "$prefix"'."ssh-authorized-keys"')
    cmdline_append=$(config_query -r "$prefix"'."cmdline-append"')
    persistence_disabled=$(config_query -r "$prefix"'."persistence-disabled"')
    if [ "$hwaddr" != "null" ]; then
        print_note " * HW address                      : $hwaddr"
    else
        host_cfg_state="ko"
        print_error "Missing hardware-ethernet property"
    fi
    if [ "$address" != "null" ]; then
        print_note " * Fixed address                   : $address"
    else
        host_cfg_state="ko"
        print_error "Missing fixed-address property"
    fi
    if [ "$host_cfg_state" == "ko" ]; then
        print_warning "Skipping this host due to previous errors"
        continue
    fi
    cat >> $LIVEBOOTP_DHCPD_CONF << EOF
    host $host {
        hardware ethernet $hwaddr;
        fixed-address $address;
    }
EOF
    cat >> $LIVEBOOTP_SSH_HOST_CONFIG << EOF
Host $host
HostName $address
User root

EOF
    if [ "$image" != "null" ]; then
        print_note " * Image name                      : $image"
        if [[ "$image" =~ $URL_PATTERN ]]; then
            image_url="$image"
        else
            image_url="$LIVEBOOTP_SQUASHFS_REPO_URL/$image"
        fi
    else
        print_warning " * No image-name property found, skipping bootp part for this host..."
        continue
    fi
    if [ "$ssh_auth_keys" != "null" ]; then
        print_note " * SSH authorized keys file used   : $ssh_auth_keys"
        if [[ "$ssh_auth_keys" =~ $URL_PATTERN ]]; then
            ssh_auth_keys_path="$ssh_auth_keys"
        else
            ssh_auth_keys_path="$LIVEBOOTP_SSH_AUTHORIZED_KEYS_BASE_URL/$ssh_auth_keys"
        fi
    else
        print_critical "Missing ssh-authorized-keys property"
    fi
    mkdir -p $LIVEBOOTP_TFTPROOT/pxelinux.cfg
    cat > "$LIVEBOOTP_TFTPROOT/pxelinux.cfg/01-${hwaddr//:/-}" << EOF
UI menu.c32
PROMPT 0

MENU TITLE Boot Menu
TIMEOUT 10
DEFAULT liveboot

LABEL liveboot
        MENU LABEL Live boot image $image
        LINUX images/$image/vmlinuz
        APPEND initrd=images/$image/initrd.img boot=live dhcp hostname=$host fetch=$image_url/rootfs.squashfs fetch-ssh-authorized-keys=$ssh_auth_keys_path $(test "$persistence_disabled" == "true" || echo "persistence") $cmdline_append $(test ! -f images/$image/cmdline || cat images/$image/cmdline | tr '\n' ' ')

EOF
done
echo "}" >> $LIVEBOOTP_DHCPD_CONF

cat > /etc/nginx/conf.d/default.conf <<EOF
server {
        listen $LIVEBOOTP_IP:80;

        root /var/lib/tftpboot;
        index index.html;

        server_name $LIVEBOOTP_IP $LIVEBOOTP_CN;

        location / {
                autoindex on;
                try_files \$uri \$uri/ =404;
        }
}
EOF

# DHCP conf sanity check
if ! /usr/sbin/dhcpd -t -q -cf "$LIVEBOOTP_DHCPD_CONF" > /dev/null 2>&1; then
    print_critical "dhcpd self-test failed. Please fix $LIVEBOOTP_DHCPD_CONF." \
                "The error was: $(/usr/sbin/dhcpd -t -cf "$LIVEBOOTP_DHCPD_CONF" 2>&1)"
fi
touch /var/lib/dhcp/dhcpd.leases
print_info "Read configuration file [DONE]"

LIVEBOOTP_DHCPD_PID="/run/dhcpd.pid"
case "$1" in
    start)
        if [ -e "/run/rsyslogd.pid" ]; then
            print_critical "Already started, please use rotate/reload action, otherwise restart docker instance"
        fi
        start-stop-daemon --start --quiet --pidfile /run/rsyslogd.pid \
                --exec /usr/sbin/rsyslogd --
        mkdir -p /run/nginx
        start-stop-daemon --start --quiet --pidfile /run/nginx/nginx.pid \
                --exec /usr/sbin/nginx -- -c /etc/nginx/nginx.conf
        start-stop-daemon --start --quiet --exec /usr/sbin/in.tftpd -- \
                --listen --user tftp --address $LIVEBOOTP_IP:69 \
                --secure $LIVEBOOTP_TFTPROOT
        start-stop-daemon --start --quiet --pidfile "$LIVEBOOTP_DHCPD_PID" \
                --exec /usr/sbin/dhcpd -- \
                -cf "$LIVEBOOTP_DHCPD_CONF" -pf "$LIVEBOOTP_DHCPD_PID" $LIVEBOOTP_INTF
        while [ -e /run/rsyslogd.pid ]; do
            tail -f /var/log/messages
        done
        ;;
    rotate)
        start-stop-daemon --stop --signal HUP --quiet --pidfile /run/rsyslogd.pid \
                --exec /usr/sbin/rsyslogd
        ;;
    reload)
        start-stop-daemon --stop --quiet --pidfile "$LIVEBOOTP_DHCPD_PID"
        sleep 2
        start-stop-daemon --start --quiet --pidfile "$LIVEBOOTP_DHCPD_PID" \
                --exec /usr/sbin/dhcpd -- \
                -cf "$LIVEBOOTP_DHCPD_CONF" -pf "$LIVEBOOTP_DHCPD_PID" $LIVEBOOTP_INTF
        ;;
esac
